# HomeAssistant VM setup

This playbook will
- setup docker
- setup docker-compose
- setup oh-my-zsh with modifiable .zshrc file
- add cron jobs for rebooting vms
- add user to dialout group

**NOTE:** This has been only tested with Ubuntu server 18.04 LTS.

The output in the server looks like this when you start a new terminal session.

```
 _____         _
|_   _|__  ___| |_
  | |/ _ \/ __| __|
  | |  __/\__ \ |_
  |_|\___||___/\__|

 __________
< Welcome! >
 ----------
   \
    \
        .--.
       |o_o |
       |:_/ |
      //   \ \
     (|     | )
    /'\_   _/`\
    \___)=(___/
```

1. Setup Ansible control-node according to the official Ansible [documentation](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-ansible)
2. Setup control-node hosts file (`/etc/ansible/hosts`) to match the IP of the wanted server
3. Put your SSH key into the server's `authorized_keys` file
4. Clone this repository to the control-node
5. Go into `console-setup` folder
6. Ensure all the variables are set accordingly in the `oh-my-zsh.yml` file

```
  hosts: servers         # to match with your /etc/ansible/hosts file
  remote_user: testusr   # to match with your server's user
  become: yes
  become_method: sudo    # to match the privilege escalation method
  become_user: root      # to match the root user
  vars:
    user:
      username: testusr  # to match with your server's user
    oh_my_zsh:
      theme: rkj-repos   # to match your wanted oh-my-zsh theme
      plugins:           # append your wanted plugins to the list
        - git
        - docker
        - docker-compose
    server_msg:
      figlet: Test       # to match your wanted message to be displayed on the server
      cowsay: Welcome!   # to match your wanted Tux message to be displayed on the server
```

7. Run the playbook

    ansible-playbook oh-my-zsh.yml -K

8. Type in your sudo password and press enter
